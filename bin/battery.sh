#!/bin/bash
full0=$(cat /sys/class/power_supply/BAT0/energy_full)
now0=$(cat /sys/class/power_supply/BAT0/energy_now)

full1=$(cat /sys/class/power_supply/BAT1/energy_full)
now1=$(cat /sys/class/power_supply/BAT1/energy_now)

full=$((full0+full1))
now=$((now0+now1))
level=$((now*100/full))

threshold=15
diff=$((threshold-level))

bat0=$(cat /sys/class/power_supply/BAT0/uevent | grep POWER_SUPPLY_STATUS)
bat1=$(cat /sys/class/power_supply/BAT1/uevent | grep POWER_SUPPLY_STATUS)

if [[ $bat0 == "POWER_SUPPLY_STATUS=Charging" || $bat1 == "POWER_SUPPLY_STATUS=Charging" ]];then
  exit 0;
fi

if (( level < threshold )); then
notify-send "Battery low !!!
🪫🪫🪫🪫🪫🪫🪫🪫🪫
"
for i in $(seq 1 $diff);do
  paplay /usr/share/sounds/gnome/default/alerts/bark.ogg&
  sleep .5
done
fi
